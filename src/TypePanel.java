/**
 * Created by Dmitry Vereykin on 7/27/2015.
 */
import javax.swing.*;
import java.awt.*;

public class TypePanel extends JPanel {
    public final double GENERAL = 895;
    public final double STUDENT = 495;

    private JRadioButton general;
    private JRadioButton student;
    private ButtonGroup bg;

    public TypePanel() {
        setLayout(new GridLayout(2, 1));

        general = new JRadioButton("General --- $895", true);
        student = new JRadioButton("Student --- $495");

        bg = new ButtonGroup();
        bg.add(general);
        bg.add(student);

        setBorder(BorderFactory.createTitledBorder("Type"));

        add(general);
        add(student);
    }

    public double getTypeCost() {
        double styleCost;

        if (general.isSelected())
            styleCost = GENERAL;
        else
            styleCost = STUDENT;
   
      return styleCost;
   }
}