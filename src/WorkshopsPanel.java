/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;

public class WorkshopsPanel extends JPanel {
    public final double E_COMMERCE = 295;
    public final double FUTURE_OF_WEB = 295;
    public final double ADVANCED_JAVA = 395;
    public final double NETWORK_SECURITY = 395;

    private JCheckBox eCommerce;
    private JCheckBox futureOfWeb;
    private JCheckBox advancedJava;
    private JCheckBox networkSecurity;


    public WorkshopsPanel() {
        setLayout(new GridLayout(4, 1));

        eCommerce = new JCheckBox("Introduction to E-commerce.........$295");
        futureOfWeb = new JCheckBox("The Future of the Web....................$295");
        advancedJava = new JCheckBox("Advanced Java Programming......$395");
        networkSecurity = new JCheckBox("Network Security.............................$395");

        setBorder(BorderFactory.createTitledBorder("Workshops"));

        add(eCommerce);
        add(futureOfWeb);
        add(advancedJava);
        add(networkSecurity);
    }

    public double getWorkshopsCost() {
        double addCost = 0.0;

        if (eCommerce.isSelected())
            addCost += E_COMMERCE;
        if (futureOfWeb.isSelected())
            addCost += FUTURE_OF_WEB;
        if (advancedJava.isSelected())
            addCost += ADVANCED_JAVA;
        if (networkSecurity.isSelected())
            addCost += NETWORK_SECURITY;

        return addCost;
    }
}