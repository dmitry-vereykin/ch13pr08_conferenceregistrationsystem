/**
 * Created by Dmitry Vereykin on 7/27/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class ConferenceRegistrationSystem extends JFrame {
    private TypePanel type;
    private TopBanner topBanner;
    private JPanel buttonPanel;
    private JButton registerButton;
    private WorkshopsPanel workshops;
    private DinnerPanel dinner;
    private JTextField totalField;
    private JTextField registerField;

    public ConferenceRegistrationSystem() {
        setTitle("Conference Registration System");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        topBanner = new TopBanner();
        type = new TypePanel();
        workshops = new WorkshopsPanel();
        dinner = new DinnerPanel();

        buildButtonPanel();

        add(topBanner, BorderLayout.NORTH);
        add(type, BorderLayout.WEST);
        add(dinner, BorderLayout.EAST);
        add(workshops, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);

        pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    private void buildButtonPanel() {
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(2, 4));

        registerButton = new JButton("Register");
        registerButton.addActionListener(new RegisterButtonListener());

        totalField = new JTextField(5);
        totalField.setHorizontalAlignment(JTextField.RIGHT);
        totalField.setEditable(false);

        registerField = new JTextField("Unregistered", 5);
        registerField.setHorizontalAlignment(JTextField.CENTER);
        registerField.setEditable(false);
        registerField.setForeground(Color.RED);

        JLabel totalLabel = new JLabel("Total: ");
        totalLabel.setHorizontalAlignment(JLabel.RIGHT);

        JLabel statusLabel = new JLabel("Status: ");
        statusLabel.setHorizontalAlignment(JLabel.RIGHT);

        buttonPanel.add(new JLabel("    "));
        buttonPanel.add(new JLabel("    "));
        buttonPanel.add(totalLabel);
        buttonPanel.add(totalField);

        buttonPanel.add(registerButton);
        buttonPanel.add(new JLabel("    "));
        buttonPanel.add(statusLabel);
        buttonPanel.add(registerField);
    }


    private class RegisterButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            double total;

            total = type.getTypeCost() + workshops.getWorkshopsCost() + dinner.getAddCost();

            DecimalFormat dollar = new DecimalFormat("0.00");

            totalField.setText("$" + dollar.format(total));
            registerField.setText("Registered");
            registerField.setForeground(Color.BLUE); //Green looks awkward
        }
    }

    public static void main(String[] args) {
        new ConferenceRegistrationSystem();
    }
}